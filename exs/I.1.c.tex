\documentclass[../main.tex]{subfiles}

\begin{document}

	O primeiro item do problema é provar que uma dada família de flechas entre 
	anéis comutativos finitamente presentáveis com unidade é uma cobertura 
	em uma categoria. Vamos agora definir algumas notações convenientes.

	\newcommand{\loc}[2]{#1[{#2}^{-1}]}
	\newcommand{\can}[1]{\operatorname {can}_{#1}}
	\newcommand{\ccur}	{\mathcal R}
	\newcommand{\dual}[1]{{#1}^{\operatorname{op}}}

	Escrevemos $\loc{R}{r}$ para a localização canônica do anel $R$ em $r$ e a 
	flecha canonicamente associada escrevemos $\function{\can r} R \loc R r$
	quando o anel $R$ estiver claro pelo contexo.

	Ainda, deixe $\ccur$ a subcategoria plena dos aneis comutativos finitamente 
	presentáveis e com unidade. Então definimos $\cat{C} = \dual \ccur$.

	As coberturas em questão são, para cada objeto $A$ de $\cat{C}$ --- que em 
	particular é um anel --- e para cada subconjunto finito $S$ de elementos de 
	$A$ cujo ideal gerado é todo o $A$, 

	\[
		\set{
			\function {\can s} {\loc A s} {A}
		}{s \in S}
	\]

	Onde estamos encarando $\can s$ como uma flecha em $\cat{C}$, não $\ccur$. Nós 
	queremos, então, provar que o mapeamento de objetos de $\cat{C}$ para a 
	coleção de coberturas de $A$ e mudança de bases destas por isomorfismos é 
	uma (base de) topologia de Grothendieck. Para tanto, vejamos o seguinte:

	\paragraph{Identidade:}
	É óbvio que o ideal gerado em $A$ por $1_A$ é o próprio $A$, então, o 
	unitário de $1_A$ é uma família que parametriza uma cobertura de $A$. Esta
	cobertura é exatamente
	\[
		\unitary {\function {\id_A} A A}
	\]
	a menos de isomorfismo, porque $A \cong \loc A 1$. Então isomorfismos estão 
	na cobertura de $A$, para todo objeto de $\cat{C}$.

	\paragraph{Estabilidade por Pullbacks:} 
	No que toca a estabilidade por pullbacks, lembremo-nos de que em $\ccur$ 
	temos pushforwards, então podemos escrever o pushforward do seguinte 
	diagrama em $\ccur$:
	\begin{center}
	\begin{tikzcd}
		A \arrow[d, "h"'] \arrow[r, "\can s"] & \loc A s \arrow[d, "\lambda"]\\
		B \arrow[r]                           & B\otimes_A \loc A s
	\end{tikzcd}
	\end{center}
	Pela propriedade universal da localização, temos também que $\lambda$ é a 
	única flecha que faz este diagrama comutar. Agora considere 
	\begin{center}
	\begin{tikzcd}
		A \arrow[r, "\can s"] \arrow[d, "h"]               & \loc A s \arrow[d, "\exists!\mu", dotted] \arrow[rdd, "\exists!\lambda", dotted, bend left] &                    \\
		B \arrow[r, "\can {h(s)}"] \arrow[rrd, bend right] & \loc B {h(s)} \arrow[rd, "\exists!\gamma", dotted]                                          &                    \\
														&                                                                                             & B\otimes_A\loc A s
	\end{tikzcd}
	\end{center}
	E, com isso, podemos estabelecer que $B\otimes_A\loc A s\cong\loc B{h(s)}$.
	Desta forma, encontramos um mapa $\function{\can{h(s)}} B {\loc B {h(s)}}$, 
	resta perceber que $h[S]$ ainda gera um ideal que coincide com $B$. Para 
	tanto, basta perceber que $1$ está no ideal gerado por $S$, então $1$ se 
	escreve como uma soma de $s^{k_s}*a_s$ com $a_s\in A$ e $k_s\in \mathbb N$.
	Então, aplicando-se $h$ a soma deve continuar a ser $1$, porque são 
	morfismos de anel com unidade.

	A família 
	\[\set{\function{\can{h(s)}} B {\loc B {h(s)}}}{h(s)\in h[S]}\]
	é portanto, o dual de uma cobertura de $B$, que era justamente o 
	necessário, pois se olharmos tudo que fizemos na categoria $\cat{C}$, nosso 
	pushforward vira o pullback desejado, nossa função $\function h A B$ se 
	torna uma flecha $B\to A$ e temos o axioma que almejávamos.

	\paragraph{Fechado por Composição:}
	Deixe $S\subseteq A$ finito, gerando $A$, e para cada $s\in S$, deixe $R_s$ 
	um subconjunto finito de $\loc A s$ que o gera. Queremos mostrar que a 
	família das composições compatíveis é cobertura também. As coberturas 
	envolvidas são 
	\[
		\set{
			\function {\can s} {A} {\loc A s}
		}{s \in S}
	\]
	\[
		\set{
			\function {\can r} {\loc A s} {\loc {\loc A s} r}
		}{r \in R_s}
	\]

	Cada $r\in R_s$ é igual a um $a_r/s^{k_r}$, com $a_r\in A$, pois são 
	elementos da localização de $A$ em $s$. Considere, portanto, o anel 
	$B=\loc A {(sa_r)}$, que é capaz de inverter $s$ e $a_r$, pois 
	$a_r/{sa_r}$ e $s/{sa_r}$ são exatamente seus inversos e, por isso, 
	é capaz de inverter $r$. Na verdade, temos que 
	\[
		{\loc {\loc A s} r} \cong \loc A {sa_r}
	\]
	
	Desta forma, quando encararmos as famílias de flechas em $\cat{C}$, as 
	coberturas podem ser compostas. De fato dá cobertura pois esse conjunto 
	de $\set {sa_r} {s\in S, r\in R_s}$ é finito (cada um dos $R_s$ é finito e
	há apenas finitos $s\in S$) e o ideal gerado por $sa_r$ continua contendo 
	o $1$, o que é necessário para que dê cobertura mesmo.

    \paragraph{Monotonicidade:}
    Seja $\set{\function {\can s} {A[r^-1]} {A}}{s \in S}$ uma cobertura, e
    $\set{\function {f_i} {A_i} {A}}{i \in I}$ uma família com uma
    ``tradução'' adequada

    \[
        \function {h_s} {A[s^{-1}]} {A_{i_s}}
    \]

    que comuta com as famílias. Pois então, queremos mostrar que a família
    indexada por $I$ é, de fato, uma cobertura. Ou seja, para cada $i$,
    encontrar um elemento $r_i$ de um subconjunto finito gerador $R$ de $A$ tal
    que $f_i = \can {r_i}$.
 
\end{document}
