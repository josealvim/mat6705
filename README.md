# Compilation tips

1. Remember to always compile from the root of the repository. LaTeX is
   temperamental, and accesses resources relative to *your* position, not that
   of the calling document.
