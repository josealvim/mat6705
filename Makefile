main.pdf : main.bbl
	xelatex main.tex
	while grep 'Please rerun LaTeX' main.log; do xelatex main.tex; done

main.bbl : main.bcf
	biber main

main.bcf :
	xelatex main.tex

notes : I.2.pdf

I.2.pdf : notes/I.2.tex I.2.bbl
	xelatex notes/*.tex

I.2.bbl : I.2.bcf
	biber I.2

I.2.bcf :
	xelatex notes/I.2.tex
